import React from 'react';
import { StyleSheet, Text, ImageBackground, View, Image } from 'react-native';

import { Alert } from 'react-native';
import BackgroundGeolocation from 'react-native-mauron85-background-geolocation';

import moment from 'moment';


export default class App extends React.Component {
  constructor () {
    super();
    this.state = {
      status: 'idle yo',
      posted: 0
    };
  }
  componentDidMount() {
    BackgroundGeolocation.configure({
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      stationaryRadius: 50,
      distanceFilter: 50,
      notificationTitle: 'Background tracking',
      notificationText: 'enabled',
      startOnBoot: false,
      stopOnTerminate: false,
      debug: false,
      locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
      interval: 10000,
      fastestInterval: 1000,
      activitiesInterval: 10000,
      stopOnStillActivity: false,
      url: 'https://friendly-crop.glitch.me/',
      // customize post properties
      postTemplate: {
        lattitude: '@latitude',
        longitude: '@longitude'
      }
    });

    BackgroundGeolocation.on('location', (location) => {
      // handle your locations here
      // to perform long running operation on iOS
      // you need to create background task
      BackgroundGeolocation.startTask(taskKey => {
        // execute long running task
        // eg. ajax post location
        // IMPORTANT: task has to be ended by endTask
        this.setState({
          status: 'sending fo sure'
        })
        fetch('https://friendly-crop.glitch.me/',
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            lattitude: location.latitude,
            longitude: location.longitude
          })
        })
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          this.setState({
            posted: this.state.posted + 1,
            status: 'heavens above it works'
          })
          BackgroundGeolocation.endTask(taskKey);
        })
        .catch((error) => {
          console.log(error);
          this.setState({
            status: 'it gone borked!'
          })
          BackgroundGeolocation.endTask(taskKey);
        })
      });
    });

    BackgroundGeolocation.on('stationary', (stationaryLocation) => {
      // handle stationary locations here
      Actions.sendLocation(stationaryLocation);
    });

    BackgroundGeolocation.on('error', (error) => {
      console.log('[ERROR] BackgroundGeolocation error:', error);
    });

    BackgroundGeolocation.on('start', () => {
      console.log('[INFO] BackgroundGeolocation service has been started');
    });

    BackgroundGeolocation.on('stop', () => {
      console.log('[INFO] BackgroundGeolocation service has been stopped');
    });

    BackgroundGeolocation.on('authorization', (status) => {
      console.log('[INFO] BackgroundGeolocation authorization status: ' + status);
      if (status !== BackgroundGeolocation.AUTHORIZED) {
        // we need to set delay or otherwise alert may not be shown
        setTimeout(() =>
          Alert.alert('App requires location tracking permission', 'Would you like to open app settings?', [
            { text: 'Yes', onPress: () => BackgroundGeolocation.showAppSettings() },
            { text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel' }
          ]), 1000);
      }
    });

    BackgroundGeolocation.on('background', () => {
      console.log('[INFO] App is in background');
    });

    BackgroundGeolocation.on('foreground', () => {
      console.log('[INFO] App is in foreground');
    });

    BackgroundGeolocation.checkStatus(status => {
      console.log('[INFO] BackgroundGeolocation service is running', status.isRunning);
      console.log('[INFO] BackgroundGeolocation services enabled', status.locationServicesEnabled);
      console.log('[INFO] BackgroundGeolocation auth status: ' + status.authorization);

      // you don't need to check status before start (this is just the example)
      if (!status.isRunning) {
        BackgroundGeolocation.start(); //triggers start on start event
      }
    });

    // you can also just start without checking for status
    // BackgroundGeolocation.start();
  }

  componentWillUnmount() {
    // unregister all event listeners
    BackgroundGeolocation.events.forEach(event => BackgroundGeolocation.removeAllListeners(event));
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground style={styles.container} source={require('./seigaihab.png')}>
          <Text style={styles.header}>Good Morning Jolandi</Text>
          <Image source={require('./radar.gif')} />
          <Text style={styles.text}>Posted: {this.state.posted} {this.state.status}</Text>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    fontSize: 20,
    color: '#0c5a6e'
  },
  text: {
    backgroundColor: 'white',
    padding: 2,
    fontSize: 18
  },
  container: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
